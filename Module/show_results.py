from Module import monotonic_regression_uncertainty as mru
from Module import tools

import multiprocessing as mp

import matplotlib.pyplot as plt
from matplotlib import patches
import colorsys
import random

from math import log




def cr_models(p, df):
    p1, p2, key = p.split('/')
    key = int(key)
    rev, up = tools.equiv_key_case(key)
    tr1 = df[p1].values.tolist()
    tr2 = df[p2].values.tolist()
    diag = df['target'].values.tolist()
    data = [((tr1[n], tr2[n] ), 1, diag[n]) for n in range(len(diag))]

    X, models = mru.compute_recursion(data, (rev, up, key))

    return p1, p2, models, data


def print_model(data, models, p1, p2, df1, pathname = None, cm=None):
    #print the monotonic space with the 3 areas

    if '.1.1' in p1:
        p1 = p1[:-2]
    if '.1.1' in p2:
        p2 = p2[:-2]

    try:
        g1 = df1[df1['Probeset_ID'] == p1]['Gene.Symbol'].values.tolist()[0]
    except:
        g1 = p1
    try:
        g2 = df1[df1['Probeset_ID'] == p2]['Gene.Symbol'].values.tolist()[0]
    except:
        g2 = p2

    for key in models.keys():
        key = int(key)
        #print('Key', key)
        plt.figure(figsize=(3,3))
        ax = plt.axes()
        ax.set_facecolor("lightgray")

        x_r = list()
        y_r = list()
        x_b = list()
        y_b = list()
        for i in range(len(data)):
            xy, w, lab = data[i]
            x, y = xy
            if lab == 0: #blue
                x_r.append(x)
                y_r.append(y)
            else: #red
                x_b.append(x)
                y_b.append(y)

        reg_err, bpr, bpb, r_p, b_p = models[key]


        min_x = min(min(x_r), min(x_b))-0.05
        min_y = min(min(y_r), min(y_b))-0.05
        max_x = max(max(x_r), max(x_b))+0.05
        max_y = max(max(y_r), max(y_b))+0.05

        for bp in bpb:
            x, y = bp
            if key == 1:
                ax.add_artist(patches.Rectangle((min_x, min_y), abs(x-min_x), abs(y-min_y), facecolor = 'lightsteelblue', zorder = 1))
            elif key == 2:
                ax.add_artist(patches.Rectangle((x, min_y), max_x+abs(x), abs(y-min_y), facecolor = 'lightsteelblue', zorder = 1))
            elif key == 3:
                ax.add_artist(patches.Rectangle((x, y), max_x+abs(x), max_y+abs(y), facecolor = 'lightsteelblue', zorder = 1))
            else:
                ax.add_artist(patches.Rectangle((min_x, y ), abs(x-min_x), max_y+abs(y), facecolor = 'lightsteelblue', zorder = 1))


        for bp in bpr:
            x, y = bp


            if key == 1:
                ax.add_artist(patches.Rectangle((x, y), max_x+abs(x), max_y+abs(y), facecolor ='lightcoral', zorder = 1))
            elif key == 2:
                ax.add_artist(patches.Rectangle((min_x, y ), abs(x-min_x), max_y+abs(y), facecolor = 'lightcoral', zorder = 1))
            elif key == 3:
                ax.add_artist(patches.Rectangle((min_x, min_y), abs(x-min_x), abs(y-min_y), facecolor = 'lightcoral', zorder = 1))
            else:
                ax.add_artist(patches.Rectangle((x, min_y), max_x+abs(x), abs(y-min_y), facecolor = 'lightcoral', zorder = 1))


        #plt.scatter(x_r, y_r, c = 'blue', marker='.', zorder = 2)
        #plt.scatter(x_b, y_b, c = 'red', marker='.', zorder = 2)

        random.shuffle(data)

        for d in data:
            if d[2] == 0:
                plt.scatter(d[0][0], d[0][1], c = 'royalblue', marker='.', zorder = 2)
            elif d[2] == 1:
                plt.scatter(d[0][0], d[0][1], c = 'firebrick', marker='.', zorder = 2)
        plt.xlabel(g1)
        plt.ylabel(g2)

        if cm is not None:
            pair = '/'.join([p1, p2, str(key)])
            error = cm.at['LOOCV', pair]
            plt.title('RE = {} & LOOCVE = {}'.format(round(reg_err/len(data),3),round(error,3)))
        else:
            plt.title('RE = {}'.format(round(reg_err,3)))

        if pathname is not None:
            plt.savefig(pathname + g1 + '_' + g2  + '.png')

            f = open(pathname + 'gene.txt', 'a')
            f.write('{} : {}\n'.format(g1, p1))
            f.write('{} : {}\n'.format(g2, p2))
            f.close()
        else:
            plt.show()




def print_model_out(data, out, models, p1, p2, df1, pathname = None):
    #print the monotonic space with the 3 areas

    if '.1.1' in p1:
        p1 = p1[:-2]
    if '.1.1' in p2:
        p2 = p2[:-2]

    try:
        g1 = df1[df1['Probeset_ID'] == p1]['Gene.Symbol'].values.tolist()[0]
    except:
        g1 = p1
    try:
        g2 = df1[df1['Probeset_ID'] == p2]['Gene.Symbol'].values.tolist()[0]
    except:
        g2 = p2

    for key in models.keys():
        key = int(key)
        print('Key', key)
        plt.figure(figsize=(5,5))
        ax = plt.axes()
        ax.set_facecolor("lightgray")

        x_r = list()
        y_r = list()
        x_b = list()
        y_b = list()
        for i in range(len(data)):
            xy, w, lab = data[i]
            x, y = xy
            if lab == 0: #blue
                x_r.append(x)
                y_r.append(y)
            else: #red
                x_b.append(x)
                y_b.append(y)

        reg_err, bpr, bpb, r_p, b_p = models[key]

        for bp in bpb:
            x, y = bp
            if key == 1:
                ax.add_artist(patches.Rectangle((0.0, 0.0), x, y, facecolor = 'lightskyblue', zorder = 1))
            elif key == 2:
                ax.add_artist(patches.Rectangle((x, 0), 1000, y, facecolor = 'lightskyblue', zorder = 1))
            elif key == 3:
                ax.add_artist(patches.Rectangle((x, y), 1000, 1000, facecolor = 'lightskyblue', zorder = 1))
            else:
                ax.add_artist(patches.Rectangle((0, y ), x, 1000, facecolor = 'lightskyblue', zorder = 1))


        for bp in bpr:
            x, y = bp


            if key == 1:
                ax.add_artist(patches.Rectangle((x, y), 1000, 1000, facecolor ='lightcoral', zorder = 1))
            elif key == 2:
                ax.add_artist(patches.Rectangle((0, y ), x, 1000, facecolor = 'lightcoral', zorder = 1))
            elif key == 3:
                ax.add_artist(patches.Rectangle((0.0, 0.0), x, y, facecolor = 'lightcoral', zorder = 1))
            else:
                ax.add_artist(patches.Rectangle((x, 0), 1000, y, facecolor = 'lightcoral', zorder = 1))


        #plt.scatter(x_r, y_r, c = 'blue', zorder = 2)
        #plt.scatter(x_b, y_b, c = 'red', zorder = 2)

        random.shuffle(data)

        for d in data:
            if d[2] == 0:
                plt.scatter(d[0][0], d[0][1], c = 'blue', marker='.', zorder = 2)
            elif d[2] == 1:
                plt.scatter(d[0][0], d[0][1], c = 'red', marker='.', zorder = 2)
        plt.scatter(out[0], out[1], c = 'g', zorder = 2)
        plt.xlabel(g1)
        plt.ylabel(g2)

        if pathname is not None:
            plt.savefig(pathname + g1 + '_' + g2  + '.png')

            f = open(pathname + 'gene.txt', 'a')
            f.write('{} : {}\n'.format(g1, p1))
            f.write('{} : {}\n'.format(g2, p2))
            f.close()
        else:
            plt.show()






def print_model_positive(data, models, p1, p2, pathname, cm):


    for key in models.keys():
        key = int(key)
        #print('Key', key)
        plt.figure(figsize=(3,3))
        ax = plt.axes()
        ax.set_facecolor("lightcoral")

        x_r = list()
        y_r = list()
        x_b = list()
        y_b = list()
        for i in range(len(data)):
            xy, w, lab = data[i]
            x, y = xy
            if lab == 0: #blue
                x_r.append(x)
                y_r.append(y)
            else: #red
                x_b.append(x)
                y_b.append(y)

        reg_err, bpr, bpb, r_p, b_p = models[key]


        min_x = min(min(x_r), min(x_b))-0.05
        min_y = min(min(y_r), min(y_b))-0.05
        max_x = max(max(x_r), max(x_b))+0.05
        max_y = max(max(y_r), max(y_b))+0.05

        for bp in bpb:
            x, y = bp
            if key == 1:
                ax.add_artist(patches.Rectangle((min_x, min_y), abs(x-min_x), abs(y-min_y), facecolor = 'lightsteelblue', zorder = 1))
            elif key == 2:
                ax.add_artist(patches.Rectangle((x, min_y), max_x+abs(x), abs(y-min_y), facecolor = 'lightsteelblue', zorder = 1))
            elif key == 3:
                ax.add_artist(patches.Rectangle((x, y), max_x+abs(x), max_y+abs(y), facecolor = 'lightsteelblue', zorder = 1))
            else:
                ax.add_artist(patches.Rectangle((min_x, y ), abs(x-min_x), max_y+abs(y), facecolor = 'lightsteelblue', zorder = 1))




        random.shuffle(data)

        for d in data:
            if d[2] == 0:
                plt.scatter(d[0][0], d[0][1], c = 'royalblue', marker='.', zorder = 2)
            elif d[2] == 1:
                plt.scatter(d[0][0], d[0][1], c = 'firebrick', marker='.', zorder = 2)
        plt.xlabel(p1)
        plt.ylabel(p2)

        if cm is not None:
            pair = '/'.join([p1, p2, str(key)])
            error = cm.at['LOOCV', pair]
            plt.title('RE = {} & LOOCVE = {}'.format(round(reg_err/len(data),3),round(error,3)))
        else:
            plt.title('RE = {}'.format(round(reg_err/len(data),3)))

        if pathname is not None:
            name = pathname + '/' + str(p1) + '_' + str(p2)  + '.pdf'
            plt.savefig(name.replace('//', '/'), bbox_inches='tight')
        else:
            plt.show()

def print_model_negative(data, models, p1, p2, pathname, cm):

    for key in models.keys():
        key = int(key)
        #print('Key', key)
        plt.figure(figsize=(3,3))
        ax = plt.axes()
        ax.set_facecolor("lightsteelblue")

        x_r = list()
        y_r = list()
        x_b = list()
        y_b = list()
        for i in range(len(data)):
            xy, w, lab = data[i]
            x, y = xy
            if lab == 0: #blue
                x_r.append(x)
                y_r.append(y)
            else: #red
                x_b.append(x)
                y_b.append(y)

        reg_err, bpr, bpb, r_p, b_p = models[key]


        min_x = min(min(x_r), min(x_b))-0.1
        min_y = min(min(y_r), min(y_b))-0.1
        max_x = max(max(x_r), max(x_b))+0.1
        max_y = max(max(y_r), max(y_b))+0.1


        for bp in bpr:
            x, y = bp


            if key == 1:
                ax.add_artist(patches.Rectangle((x, y), max_x+abs(x), max_y+abs(y), facecolor ='lightcoral', zorder = 1))
            elif key == 2:
                ax.add_artist(patches.Rectangle((min_x, y ), abs(x-min_x), max_y+abs(y), facecolor = 'lightcoral', zorder = 1))
            elif key == 3:
                ax.add_artist(patches.Rectangle((min_x, min_y), abs(x-min_x), abs(y-min_y), facecolor = 'lightcoral', zorder = 1))
            else:
                ax.add_artist(patches.Rectangle((x, min_y), max_x+abs(x), abs(y-min_y), facecolor = 'lightcoral', zorder = 1))


        random.shuffle(data)

        for d in data:
            if d[2] == 0:
                plt.scatter(d[0][0], d[0][1], c = 'royalblue', marker='.', zorder = 2)
            elif d[2] == 1:
                plt.scatter(d[0][0], d[0][1], c = 'firebrick', marker='.', zorder = 2)
        plt.xlabel(p1)
        plt.ylabel(p2)

        if cm is not None:
            pair = '/'.join([p1, p2, str(key)])
            error = cm.at['LOOCV', pair]
            plt.title('RE = {} & LOOCVE = {}'.format(round(reg_err/len(data),3),round(error,3)))
        else:
            plt.title('RE = {}'.format(round(reg_err/len(data),3)))

        if pathname is not None:
            name = pathname + '/' + str(p1) + '_' + str(p2)  + '.pdf'
            plt.savefig(name.replace('//', '/'), bbox_inches='tight')
        else:
            plt.show()


def show_results_pos(df, pairs, nbcpus, pathname, cm):
    pool = mp.Pool(nbcpus)
    vals = [(p, df) for p in pairs]

    res = pool.starmap(cr_models, vals, max(1,len(vals)//nbcpus) )

    for r in res:
        p1, p2, models, data = r
        print_model_positive(data, models, p1, p2, pathname, cm)


def show_results_neg(df, pairs, nbcpus, pathname, cm):
    pool = mp.Pool(nbcpus)
    vals = [(p, df) for p in pairs]

    res = pool.starmap(cr_models, vals, max(1,len(vals)//nbcpus) )

    for r in res:
        p1, p2, models, data = r
        print_model_negative(data, models, p1, p2, pathname, cm)

def show_results_RS(df, probs_df, pairs, nbcpus, pathname, cm):
    pool = mp.Pool(nbcpus)
    vals = [(p, df) for p in pairs]

    res = pool.starmap(cr_models, vals, max(1,len(vals)//nbcpus) )

    for r in res:
        p1, p2, models, data = r
        print_model_RS(data, models, p1, p2, probs_df, pathname, cm)
