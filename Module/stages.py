from Module import cost_matrix_uncertainty as cmu
from Module import optimal_k_aggregations as oka
from Module import monotonic_regression_uncertainty as mru
from Module import tools
from Module import selection_algorithm as sa
from Module import dynamic_preselection as dpsl

import pandas as pd
import matplotlib.pyplot as plt
import os




def stage0(df, nbcpus, m):
    config = dpsl.all_configurations(df)
    Q, pairs = dpsl.algorithm_2(config, df, m, nbcpus)
    return pairs




def stage_1(df, cls, min_k, max_k, nbcpus, strat, funct):
    k_error = oka.k_missclassification(df, cls, nbcpus, funct, strat, min_k, max_k)
    k_opt, err_k = oka.optimal_k(k_error)
    return k_opt, k_error



def stage_2(df, cls, k_opt, auc_file, nbcpus, funct, strat):
    errors, y_true, y_pred, y_proba = list(), list(), list(), list()

    for i in range(len(df)):
        out = df.iloc[i, :]
        df_2 = df.drop([i])
        df_2.reset_index(drop=True, inplace=True)

        ndf_err = cmu.error_matrix(df_2, cls, nbcpus,funct)
        cost = cmu.cost_classifiers(ndf_err)


        mve, pairs, algo = oka.find_k_metamodel(df_2, ndf_err, cost, k_opt, nbcpus, strat)
        pred, proba = oka.create_and_predict_metamodel(df_2, out, pairs, nbcpus, funct)

        errors.append(abs(out['target']-pred))
        y_proba.append(proba)
        y_true.append(out['target'])
        y_pred.append(pred)


    acc = errors.count(0)/len(errors)
    auc = tools.auc_score(y_proba, y_true, auc_file)
    ci = tools.confidence_interval(auc, y_true)

    labels, probas, uncertain_pts = tools.unclassified_points(y_true, y_proba)
    return acc, auc, ci

def stage_3(df, cls, k_opt, nbcpus, funct, strat):
    ndf_err = cmu.error_matrix(df, cls, nbcpus,funct)
    cost = cmu.cost_classifiers(ndf_err)
    mve, pairs, algo = oka.find_k_metamodel(df, ndf_err, cost, k_opt, nbcpus, strat)
    return pairs, ndf_err
