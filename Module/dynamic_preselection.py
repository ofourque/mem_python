import pandas as pd
import numpy as np
from random import shuffle
import pandas as pd
from Module import monotonic_regression_uncertainty as mru
from Module import tools
import heapq as hq
from copy import deepcopy

import multiprocessing as mp

import time

def monotonic_model_RE(p, df):
    p1, p2, key = p.split('/')
    key = int(key)
    rev, up = tools.equiv_key_case(key)
    tr1 = df[p1].values.tolist()
    tr2 = df[p2].values.tolist()
    diag = df['target'].values.tolist()
    data = [((tr1[n], tr2[n] ), 1, diag[n]) for n in range(len(diag))]
    X, m = mru.compute_recursion(data, (rev, up, key))
    reg, bpr, bpb, pr, pb = m[key]
    return (reg, p)


def H_df(df, cls, nbcpus):
    pool = mp.Pool(nbcpus)
    vals = [(c, df) for c in cls]
    res = pool.starmap(monotonic_model_RE, vals, max(1,len(vals)//nbcpus))

    return sorted(res)


def monotonic_model_LOOCV(p, df, maxi):
    p1, p2, key = p.split('/')
    key = int(key)
    rev, up = tools.equiv_key_case(key)
    tr1 = df[p1].values.tolist()
    tr2 = df[p2].values.tolist()
    diag = df['target'].values.tolist()
    data = [((tr1[n], tr2[n] ), 1, diag[n]) for n in range(len(diag))]

    err = 0


    for d in data:
        data_bis = deepcopy(data)
        data_bis.remove(d)
        X, m = mru.compute_recursion(data_bis, (rev, up, key))


        target = d[2]
        out = d[0]

        reg, bpr, bpb, rps, bps = m[key]
        pred = mru.predict_severe(out, bpr, bpb, rev, up)

        err += abs(target-pred)
        if err > maxi:
            return (maxi+1, p)

    return (err, p)


def Q_df(df, cls, nbcpus, maxi):
    pool = mp.Pool(nbcpus)
    vals = [(c, df, maxi) for c in cls]
    return sorted(pool.starmap(monotonic_model_LOOCV, vals, max(1,len(vals)//nbcpus)))


def heapify(arr, n, i):
    # Find the largest among root, left child and right child
    largest = i
    l = 2 * i + 1
    r = 2 * i + 2

    if l < n and arr[i][0] < arr[l][0]:
        largest = l

    if r < n and arr[largest][0] < arr[r][0]:
        largest = r

    # Swap and continue heapifying if root is not largest
    if largest != i:
        arr[i], arr[largest] = arr[largest], arr[i]
        heapify(arr, n, largest)


# Function to insert an element into the tree
def insert(array, newNum):
    size = len(array)+1
    if size == 0:
        array.append(newNum)
    else:
        array.append(newNum)
        for i in range((size // 2) - 1, -1, -1):
            heapify(array, size, i)


# Function to delete an element from the tree
def deleteNode(array, num):
    size = len(array)
    i = 0
    for i in range(0, size):
        if num == array[i][0]:
            break


    array[i], array[size - 1] = array[size - 1], array[i]

    array.remove(array[size - 1])


    for i in range((len(array) // 2) - 1, -1, -1):
        heapify(array, len(array), i)


def deleteNodeNum(array, num):
    size = len(array)
    i = 0
    for i in range(0, size):
        if num == array[i][0]:
            break

    array[i], array[size - 1] = array[size - 1], array[i]
    array.remove(array[size - 1])
    for i in range((len(array) // 2) - 1, -1, -1):
        heapify(array, len(array), i)

def deleteNodeName(array, name):
    size = len(array)
    i = 0
    for i in range(0, size):
        if name == array[i][1]:
            break

    array[i], array[size - 1] = array[size - 1], array[i]
    array.remove(array[size - 1])

    for i in range((len(array) // 2) - 1, -1, -1):
        heapify(array, len(array), i)



def keepPairs(Q, pair, q):
    g1, g2, g3 = pair.split('/')
    r1 = lookGene(Q, g1)
    r2 = lookGene(Q, g2)

    f1 = True
    f2 = True

    pairs = list()

    if r1[0]:
        if q >= r1[1][0]:
            f1 = False
        pairs.append(r1[1])

    if r2[0]:
        if q >= r2[1][0]:
            f2 = False
        pairs.append(r2[1])

    if f1 and f2:
        return True, pairs
    else:
        return False, pairs

def lookGene(Q, gene):
    flag = False
    pair_with_gene = None
    for el in Q:
        p1, p2, p3 = el[1].split('/')
        if gene == p1 or gene == p2:
            flag = True
            pair_with_gene = el
            break
    return flag, pair_with_gene


def suppH(H, num):
    j = 0
    while H[j][0] < num:
        j+=1
    return H[:j]




def nb_de_genes(G):
    s = set()
    for k in G.keys():
        s = s.union(G[k])
    return len(s)


def H_dict(H):
    Hd = dict()
    for h in H:
        kh = h[0]
        if kh not in Hd.keys():
            Hd[kh] = list()
        Hd[kh].append(h[1])
    return Hd


def Q_dict(Q):
    Qd = dict()
    G = dict()
    for q in Q:
        kq = q[0]
        g1, g2, g3 = q[1].split('/')
        if kq not in Qd.keys():
            Qd[kq] = set()
        if kq not in G.keys():
            G[kq] = set()
        Qd[kq].add(q[1])
        G[kq].add(g1)
        G[kq].add(g2)


    return Qd, G

def update_dict(G, G_):
    for key in G_.keys():
        if key not in G.keys():
            G[key] = G_[key]
        else:
            G[key] = G[key].union(G_[key])
    return G


def supp_H_above_a(H, a):
    S = [k for k in H.keys() if k > a]
    for s in S:
        del H[s]
    return H

def supp_H_below_a(H, a):
    S = [k for k in H.keys() if k <= a]
    for s in S:
        del H[s]
    return H


def check_disjoint_pairs(Q, param):
    dis_p = list()
    genes = list()

    flag = False
    for k in sorted(Q.keys()):
        pairs = Q[k]
        for p in pairs:
            g1, g2, g3 = p.split('/')
            if g1 not in genes and g2 not in genes:
                dis_p.append(p)
                genes.append(g1)
                genes.append(g2)
                if len(dis_p) >= param:
                    flag = True
                    break
    return flag

### Preselection based on the number of genes
def algorithm_1(cls, df, k, nbcpus):

    t0 = time.time()
    H = H_df(df, cls, nbcpus)
    t1 = time.time()



    Hd = H_dict(H)

    Q = dict() #For each strat of LOOCV, we have a list of pairs
    G = dict() #List of genes in Q

    count = 0



    t2 = time.time()


    for h_key in sorted(Hd.keys()):

        pairs = Hd[h_key]
        Q_ = Q_df(df, pairs, nbcpus, max(Hd.keys()))
        Qd, Gd = Q_dict(Q_)


        G = update_dict(G, Gd)
        Q = update_dict(Q, Qd)


        if nb_de_genes(G) >=k:
            break


    a = max(Q.keys())
    G_ = deepcopy(G)
    del G_[a]
    while nb_de_genes(G) > k and nb_de_genes(G_) >= k:
        G = G_
        del Q[a]
        a = max(Q.keys())
        G_ = deepcopy(G)
        del G_[a]




    a = max(Q.keys()) #Highest value of LOOCV in Q
    Hd = supp_H_above_a(Hd, a)
    Hd = supp_H_below_a(Hd, h_key)




    for h_key in sorted(Hd.keys()):
        a = max(Q.keys())
        if h_key <= a:

            pairs = Hd[h_key]
            Q_ = Q_df(df, pairs, nbcpus, a)
            Qd, Gd = Q_dict(Q_)

            Qd = supp_H_above_a(Qd, a)



            Gd = supp_H_above_a(Gd, a)



            G = update_dict(G, Gd)
            Q = update_dict(Q, Qd)


            G_ = deepcopy(G)
            del G_[a]
            while nb_de_genes(G) > k and nb_de_genes(G_) >= k:
                G = G_
                del Q[a]
                a = max(Q.keys())
                G_ = deepcopy(G)
                del G_[a]

    pairs = list()
    for key in Q.keys():
        pairs += Q[key]


    return Q, pairs




## Preselection based on the number of pairs

def algorithm_2(cls, df, m, nbcpus):

    t0 = time.process_time()
    H = H_df(df, cls, nbcpus)
    t1 = time.process_time()



    Hd = H_dict(H)

    Q = dict() #For each strat of LOOCV, we have a list of pairs


    count = 0


    t2 = time.process_time()


    for h_key in sorted(Hd.keys()):

        pairs = Hd[h_key]
        Q_ = Q_df(df, pairs, nbcpus, max(Hd.keys()))
        Qd, Gd = Q_dict(Q_)

        Q = update_dict(Q, Qd)


        if check_disjoint_pairs(Q, m):
            break


    a = max(Q.keys()) #Highest value of LOOCV in Q
    Hd = supp_H_above_a(Hd, a)
    Hd = supp_H_below_a(Hd, h_key)


    t3 = time.process_time()


    t4 = time.process_time()

    for h_key in sorted(Hd.keys()):
        a = max(Q.keys())
        if h_key <= a:

            pairs = Hd[h_key]
            Q_ = Q_df(df, pairs, nbcpus, a)
            Qd, Gd = Q_dict(Q_)

            Qd = supp_H_above_a(Qd, a)

            Q = update_dict(Q, Qd)

            Q_ = deepcopy(Q)
            del Q_[a]

            while check_disjoint_pairs(Q_, m):

                Q = Q_
                a = max(Q.keys())

                Q_ = deepcopy(Q)
                del Q_[a]

    pairs = list()
    for key in Q.keys():
        pairs += Q[key]

    t5 = time.process_time()


    return Q, pairs


def all_configurations(df):
    transcripts = list(df.columns)
    transcripts.remove('target')

    configurations = list()
    for i in range(len(transcripts)):
        for j in range(i+1, len(transcripts)):
            for key in range(1,5):
                configurations.append('/'.join([transcripts[i], transcripts[j], str(key)]))
    return configurations
